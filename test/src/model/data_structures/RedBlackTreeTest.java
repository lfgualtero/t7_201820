package model.data_structures;

import java.util.ArrayList;
import java.util.Iterator;

import junit.framework.TestCase;

public class RedBlackTreeTest extends TestCase {
	
	//--------------------------------------------------------
	//Atributos
	//--------------------------------------------------------
	

	private RedBlackTree<Integer, String> tree;
	
	
	//------------------------------------------------------
	//Escenarios
	//------------------------------------------------------

	public void setUpEscenario0() {
		tree = new RedBlackTree<Integer, String>();
	}  
	
	public void setUpEscenario1() {
		setUpEscenario0();
		tree.put(30, "Paola");
		tree.put(10, "Andrea");
		tree.put(50, "Andres");
		tree.put(4, "Daniel");
		tree.put(20, "David");
	}
	
	public void setUpEscenario2() {
		setUpEscenario0();
		tree.put(30, "Paola");
		tree.put(10, "Andrea");
		tree.put(50, "Andres");
		tree.put(4, "Daniel");
		tree.put(20, "David");
		
		tree.put(130, "Paola");
		tree.put(110, "Andrea");
		tree.put(150, "Andres");
		tree.put(14, "Daniel");
		tree.put(120, "David");

	}


	//------------------------------------------------------
	//Metodos de prueba
	//------------------------------------------------------

	public void testAgregarVariosElementos() {
		setUpEscenario0();
		
		tree.put(30, "Paola");
		tree.put(10, "Andrea");
		tree.put(50, "Andres");
		tree.put(4, "Daniel");
		tree.put(20, "David");
		
		System.out.println("Tama�o debe ser 5 ? "+ tree.size());
		
		assertTrue(tree.size() == 5);
		
		assertTrue(tree.contains(30));
		assertTrue(tree.contains(10));
		assertTrue(tree.contains(50));
		assertTrue(tree.contains(4));
		assertTrue(tree.contains(20));
		
	}
	
	public void testDarVariosElementos() {
		setUpEscenario1();
		
		assertEquals("Paola", tree.get(30));
		assertEquals("Andrea", tree.get(10));
		assertEquals("Andres", tree.get(50));
		assertEquals("Daniel", tree.get(4));
		assertEquals("David", tree.get(20));
	}
	
	public void testEliminarVariosElementos() {
		setUpEscenario1();
		assertTrue(tree.size() == 5);

		try {
			tree.delete(30);
			assertTrue(tree.size() == 4);
			tree.delete(10);
			assertTrue(tree.size() == 3);
			tree.delete(50);
			assertTrue(tree.size() == 2);
			tree.delete(4);
			assertTrue(tree.size() == 1);
			tree.delete(20);
			assertTrue(tree.size() == 0);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void testIteradorLlaves() {
		
		setUpEscenario1();
		
		Iterator<Integer> t = tree.keysIterator();
		ArrayList<Integer> n = new ArrayList<Integer>(); 
		
		while(t.hasNext()) {
			Integer in = t.next();
			System.out.println("Llave: " + in);
			n.add(in);
		}
		
		assertTrue(n.size() == 5);
			
	}
	
	public void testIteradorLlavesRango() {
		setUpEscenario1();
		Iterator<Integer> t = tree.keysInRange(9, 21);
		ArrayList<Integer> n = new ArrayList<Integer>(); 
		while(t.hasNext()) {
			Integer in = t.next();
			System.out.println("Llave: " + in);
			n.add(in);
		}
		
		assertTrue(n.size() == 2);
	}

	public void testIteradorValoresRango() {
		setUpEscenario1();
		Iterator<String> t = tree.valuesInRange(9, 21);
		ArrayList<String> n = new ArrayList<String>(); 
		while(t.hasNext()) {
			String in = t.next();
			System.out.println("Llave: " + in);
			n.add(in);
		}
		
		assertTrue(n.size() == 2);
	}
	
	public void testArbolBalanceado() {
		setUpEscenario2();
		assertTrue(tree.check());
		assertTrue(tree.nodosNegrosBalanceados());
		assertTrue(tree.noHayPadreNiHijoRojoSeguidos());
		assertTrue(tree.hijoDerechoNoEsRojo());
		assertTrue(tree.padresMayoresQueHijoIzquierdo());
		assertTrue(tree.padresMenoresQueHijoDerecho());

	}

}
