package model.logic;

import java.util.Iterator;

import api.IDivvyTripsManager;
import model.vo.VOBike;
import model.vo.VOBikeRoute;
import model.vo.VOStation;
import model.vo.VOTrip;
import model.data_structures.DoublyLinkedList;
import model.data_structures.RedBlackTree;
import model.data_structures.Stack;
public class DivvyTripsManager implements IDivvyTripsManager {
	//--------------------------------------------------------
	//Atributos
	//--------------------------------------------------------
	/*
	 * Lista de rutas
	 */
	private DoublyLinkedList<VOBikeRoute> routes;

	/*
	 * Administrador de Datos
	 */
	private DataManager dataManager;




	private RedBlackTree<Integer, VOBike> bikesRBT;


	/*
	 * Administrador de consutas sobre bicicletas
	 */
	private	Bikes bikesManager;



	private RedBlackTree<Integer, VOTrip> tripsRBT;


	/**
	 * Stack de stations
	 */
	private Stack<VOStation> stationsS;



	//--------------------------------------------------------
	//Constructor
	//--------------------------------------------------------

	public DivvyTripsManager() {
		dataManager = new DataManager();
		routes = new DoublyLinkedList<VOBikeRoute>();
		bikesRBT= new RedBlackTree<Integer, VOBike>();
		stationsS=new Stack<VOStation>();
		bikesManager = new Bikes();
		
		tripsRBT = new RedBlackTree<Integer, VOTrip>();
	}
	//--------------------------------------------------------
	//Metodos
	//--------------------------------------------------------
	public void loadBikeRoutesJSON(String jsonRoute) {
		routes= dataManager.loadBikeRoutesJSON(jsonRoute);
	}


	public void loadTrips(String tripsfile) {

		tripsRBT = dataManager.loadTrips(tripsfile, tripsRBT);

	}
	public void loadStations(String Stationsfile) {

		stationsS= dataManager.loadStations(Stationsfile, stationsS);
	}

	public void loadBikes() {

		VOTrip[] tripsArr= new VOTrip[tripsRBT.size()];
		int i=0;
		Iterator<Integer> keys = tripsRBT.keysIterator();
		
		while(keys.hasNext()) {
			tripsArr[i]= tripsRBT.get(keys.next());
			i++;
		}
		bikesRBT= bikesManager.getBikesTreeFromTrips(tripsArr,  stationsS );
	}

	public int darNumeroBikes(){
		return bikesRBT.size();
	}


	public VOBike getBike(int bikeId){
		return bikesRBT.get(bikeId);
	}
	/**
	 * Consultar los Ids de las bicicletas que se encuentren registradas con Ids en un rango dado por
	 *[Idmenor, IdMayor]. Se espera hacer una b�squeda eficiente de los Ids de las bicicletas en el rango
	 *(Debe intentarse No recorrer todo el �rbol).
	 */
	public Stack <Integer> getBikeIdsInARange(int idMenor, int idMayor){

		Iterator<Integer> iter= bikesRBT.keysInRange(idMenor, idMayor);
		Stack <Integer> respuesta= new Stack <Integer>();
		while(iter.hasNext()){
			int id=iter.next();
			respuesta.push(id);
		}

		return respuesta;
	}

	public int totalNodosBicicletas(){
		return bikesRBT.size();
	}


	public int alturaRealArbolBicicletas(){
		return bikesRBT.height();
	}


	public double alturaPromedioArbolBicicletas(){
		double totalbicletas=totalNodosBicicletas();
		double totalAltura=0.0;

		Iterator<Integer> iter= bikesRBT.keysIterator();

		while(iter.hasNext()){
			int altura=bikesRBT.getHeight(iter.next());
			totalAltura+=altura;
		}
		return totalAltura/totalbicletas;
	}

	public int alturaTeoricaRBTBicicletasMasAlto(int numeroNodos){

		return (int) Math.ceil(2*logaritmoBase2(numeroNodos+1));

	}

	public int alturaTeoricaRBTBicicletasMasBajo(int numeroNodos){

		return (int) Math.ceil(logaritmoBase2(numeroNodos+1)-1);

	}

	public int alturaTorica23TMasAlto(int numeroNodos){

		return (int) Math.floor(1 + logaritmoBase2(numeroNodos));

	}

	public int alturaTorica23TMasBajo(int numeroNodos){

		return (int) Math.floor(1 + logaritmoBase3(numeroNodos) );

	}
	//--------------------------------------------------------
	//Metodos auxiliares   
	//--------------------------------------------------------

	public double logaritmoBase2(int n){

		return (int) (Math.log(n) / Math.log(2));

	}

	public double logaritmoBase3(int n){

		return (int) (Math.log(n) / Math.log(3));

	}



}