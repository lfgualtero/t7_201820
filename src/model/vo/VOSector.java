package model.vo;

import model.data_structures.DoublyLinkedList;

public class VOSector {
	private int id;
	private double maxLongitud;
	private double minLongitud;
	private double maxLatitud;
	private double minLatitud;

	DoublyLinkedList<VOBikeRoute> cicloRutasPorSector;



	public VOSector(int id, double maxLongitud, double minLongitud, double maxLatitud, double minLatitud) {
		this.id = id;
		this.maxLongitud = maxLongitud;
		this.minLongitud = minLongitud;
		this.maxLatitud = maxLatitud;
		this.minLatitud = minLatitud;
		this.cicloRutasPorSector = new DoublyLinkedList<VOBikeRoute>();
	}



	public VOSector() {

	}




	public void setId(DoublyLinkedList<VOBikeRoute> cicloRutasPorSector) {
		this.cicloRutasPorSector = cicloRutasPorSector;
	}


	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public double getMaxLongitud() {
		return maxLongitud;
	}
	public void setMaxLongitud(double maxLongitud) {
		this.maxLongitud = maxLongitud;
	}
	public double getMinLongitud() {
		return minLongitud;
	}
	public void setMinLongitud(double minLongitud) {
		this.minLongitud = minLongitud;
	}
	public double getMaxLatitud() {
		return maxLatitud;
	}
	public void setMaxLatitud(double maxLatitud) {
		this.maxLatitud = maxLatitud;
	}
	public double getMinLatitud() {
		return minLatitud;
	}
	public void setMinLatitud(double minLatitud) {
		this.minLatitud = minLatitud;
	}
	public void setCicloRutasPorSector(DoublyLinkedList<VOBikeRoute> cicloRutasPorSector) {
		this.cicloRutasPorSector = cicloRutasPorSector;
	}
	public DoublyLinkedList<VOBikeRoute> getCicloRutasPorSector() {
		return cicloRutasPorSector;
	}
}
