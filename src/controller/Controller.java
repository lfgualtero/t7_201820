package controller;

import java.util.Iterator;

import javax.sound.midi.VoiceStatus;

import api.IDivvyTripsManager;
import model.data_structures.DoublyLinkedList;
import model.data_structures.Stack;
import model.logic.DivvyTripsManager;
import model.vo.VOBike;
import model.vo.VOBikeRoute;
import model.vo.VOSector;

public class Controller {

	/**
	 * Reference to the services manager
	 */
	@SuppressWarnings("unused")
	private static DivvyTripsManager  manager = new DivvyTripsManager();
	
	public static void loadTripsAndstations() {

		manager.loadTrips("./data/Divvy_Trips_2017_Q1.csv");
		manager.loadTrips("./data/Divvy_Trips_2017_Q2.csv");
		//manager.loadTrips("./data/Divvy_Trips_2017_Q3.csv");
		//manager.loadTrips("./data/Divvy_Trips_2017_Q4.csv");
		
		manager.loadStations( "./data/Divvy_Stations_2017_Q1Q2.csv");
		//manager.loadStations(  "./data/Divvy_Stations_2017_Q3Q4.csv");
	}
	public static void loadBikes(){
		manager.loadBikes();
	}
	
	public static VOBike getBike(int bikeId){
		return manager.getBike(bikeId);
	}
	
	public static int darNumBikes(){
		return manager.darNumeroBikes();
	}
	
	public static Stack <Integer> getBikeIdsInARange(int idMenor, int idMayor){

		return manager.getBikeIdsInARange(idMenor, idMayor);
	}
	
	public static int totalNodosBicicletas(){
		return manager.totalNodosBicicletas();
	}


	public static int alturaRealArbolBicicletas(){
		return manager.alturaRealArbolBicicletas();
	}


	public static double alturaPromedioArbolBicicletas(){
		
		return manager.alturaPromedioArbolBicicletas();
	}

	public static int alturaTeoricaRBTBicicletasMasAlto(){

		return manager.alturaTeoricaRBTBicicletasMasAlto(totalNodosBicicletas());

	}

	public static int alturaTeoricaRBTBicicletasMasBajo(){

		return manager.alturaTeoricaRBTBicicletasMasBajo( totalNodosBicicletas());

	}

	public static int alturaTorica23TMasAlto(){

		return manager.alturaTorica23TMasAlto(totalNodosBicicletas());

	}

	public static int alturaTorica23TMasBajo(){

		return manager.alturaTorica23TMasBajo( totalNodosBicicletas());

	}

}
