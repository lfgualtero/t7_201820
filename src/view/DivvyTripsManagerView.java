package view;

import java.util.Scanner;

import controller.Controller;
import model.data_structures.DoublyLinkedList;
import model.data_structures.Stack;
import model.vo.VOBike;
import model.vo.VOBikeRoute;
import model.vo.VOSector;

public class DivvyTripsManagerView 
{
	public static void main(String[] args) 
	{
		Scanner sc = new Scanner(System.in);
		boolean fin=false;
		while(!fin)
		{
			printMenu();

			int option = sc.nextInt();

			switch(option)
			{
			case 1:
				Controller.loadTripsAndstations();
				Controller.loadBikes();

				System.out.println("-----------------------------------------------------------");
				System.out.println("se cargaron  " + Controller.darNumBikes() + " bicicletas");
				System.out.println("-----------------------------------------------------------");

				break;

			case 2:
				System.out.println("Ingrese el id de la bici:");

				int num = Integer.valueOf(sc.next());
				VOBike bike1= Controller.getBike(num);
				System.out.println("Bike Id: " +bike1.darId());
				System.out.println("Distancia total recorrida: " + bike1.getTotalDistancia());
				System.out.println("Total de viajes: " + bike1.getTotalViajes());
				System.out.println("Total tiempo invertido en viajes (milisegundos) : " + bike1.getDuracionTotalViajes());


				break;

			case 3:

				System.out.println("Ingrese el Id menor : ");

				int num1 = Integer.valueOf(sc.next());

				System.out.println("Ingrese el Id mayor : ");
				int num2=Integer.valueOf(sc.next());

				Stack <Integer> in = Controller.getBikeIdsInARange( num1,  num2);
				for(Integer m : in) {
					System.out.println("Id: " + m);
				}
				System.out.println("Cantidad de bicletas encontradas: " + in.size());

				break;

			case 4:
				System.out.println("Total de bicletas(nodos): " + Controller.totalNodosBicicletas());

				break;

			case 5:
				System.out.println("Altura �rbol: " + Controller.alturaRealArbolBicicletas());
				break;
			case 6:
				System.out.println("Altura promedio: " + Controller.alturaPromedioArbolBicicletas());
				break;
			case 7:
				System.out.println("Altura te�rica del �rbol Red-Black m�s bajo: ");
				System.out.println(Controller.alturaTeoricaRBTBicicletasMasBajo());
				break;
			case 8:

				System.out.println("Altura te�rica del �rbol Red-Black m�s alto : ");
				System.out.println(Controller.alturaTeoricaRBTBicicletasMasAlto());

				break;
			case 9:
				System.out.println("Altura te�rica del �rbol 2-3 m�s bajo: ");
				System.out.println(Controller.alturaTorica23TMasBajo());

				break;
			case 10:
				System.out.println("Altura te�rica del �rbol 2-3 m�s alto: ");
				System.out.println(Controller.alturaTorica23TMasAlto());

				break;
			case 11:	
				fin = true;
				sc.close();
				break;
			}
		}
	}

	private static void printMenu() {
		System.out.println("---------ISIS 1206 - Estructuras de datos----------");
		System.out.println("---------------------Taller 7----------------------");
		System.out.println("1. Cargar viajes, estaciones y bicicletas");
		System.out.println("2. Consultar la informaci�n asociada a una bicicleta dado su Id");
		System.out.println("3. Consultar los Ids de las bicicletas que se encuentren registradas con Ids en un rango dado por (Idmenor, IdMayor)");
		System.out.println("4. Total de nodos (bicicletas) en el �rbol");
		System.out.println("5. Altura (real) del �rbol");
		System.out.println("6. Altura promedio");
		System.out.println("7. Altura te�rica del �rbol Red-Black m�s bajo ");
		System.out.println("8. Altura te�rica del �rbol Red-Black m�s alto ");
		System.out.println("9. Altura te�rica del �rbol 2-3 m�s bajo");
		System.out.println("10. Altura te�rica del �rbol 2-3 m�s alto");
		System.out.println("11. Salir");
		System.out.println("Digite el n�mero de opci�n para ejecutar la tarea, luego presione enter: (Ej., 1):");

	}
}
